class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    return if courses.include?(course)
    raise 'Already enrolled!' if has_conflict?(course)

    courses << course
    course.students << self
  end

  def course_load
    load = Hash.new(0)

    courses.each do |course|
      load[course.department] += course.credits
    end

    load
  end

  def has_conflict?(new_course)
    courses.any? do |enrolled|
      new_course.conflicts_with?(enrolled)
    end
  end

end
